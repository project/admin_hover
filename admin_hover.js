
/**
 * Fetch admin_hover divs from the menu callback
 */
function admin_hover_init(ids, module, array) {
  $.post(Drupal.settings.admin_hover.base_path + 'admin_hover/js/' + module, {admin_hover: 1, ids: ids, destination: Drupal.settings.admin_hover.destination},
    function(response) {
      response = Drupal.parseJson(response);
      if (response.status) {
        items = response.data;
        for (i = 0; i < items.length; i++) {
          div = $('#' + items[i].id);
          div.append(items[i].admin_hover).addClass('has-admin_hover').addClass(module + '-has-admin_hover');
          admin_hover_hover(div, module);
        }
      }
  });
}

/**
 * Set the hover events for admin_hover divs
 */
function admin_hover_hover(div, module) {
  hover_class = 'has-admin_hover-hover';
  module_hover_class = module + '-has-admin_hover-hover';
  div.hover(
    function () {
      id = $(this).addClass(hover_class).addClass(module_hover_class).attr('id');
      $('#' + id + '-admin_hover').fadeIn('fast');
    }, 
    function () {
      id = $(this).removeClass(hover_class).removeClass(module_hover_class).attr('id');
      $('#' + id + '-admin_hover').fadeOut('fast');
    }
  );
}

/**
 * Loop thru nodes and blocks and fecth admin_hover divs
 */
$(document).ready(function() {
  modules = Drupal.settings.admin_hover.modules;
  for (i = 0; i < modules.length; i++) {
    module = modules[i];
    ids = "";
    $('div.' + module).each(function() {
      id = $(this).attr('id');
      if (id) ids += id + ";";
    });
    if (ids.length > 0) admin_hover_init(ids, module);  
  };
});
